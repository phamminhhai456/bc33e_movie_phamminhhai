import { GROUPID, https } from "./url.config";

export const theaterService = {
  getTheater: () => {
    return https.get(
      `/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=${GROUPID}`
    );
  },
};
