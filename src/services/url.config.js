import axios from "axios";
import { store } from "../index";
import { BAT_LOAIDNG, TAT_LOADING } from "../redux/constants/SpinnerConstant";
import { userInforLocal, USER_INFOR } from "./local.service";
export const TOKEN_CYBERSOFT =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCAzM0UiLCJIZXRIYW5TdHJpbmciOiIxOS8wNC8yMDIzIiwiSGV0SGFuVGltZSI6IjE2ODE4NjI0MDAwMDAiLCJuYmYiOjE2NTQzNjIwMDAsImV4cCI6MTY4MjAxMDAwMH0.8vVBHKZZpOpTUa6ep4mWe7SQc5U-y_8IFYOnVCJLEgI";

export const BASE_URL = "https://movienew.cybersoft.edu.vn";

export const configHeaders = () => {
  return {
    TokenCybersoft: TOKEN_CYBERSOFT,
    Authorization: "Bearer " + userInforLocal.get()?.accessToken,
  };
};

export const https = axios.create({
  baseURL: BASE_URL,
  headers: configHeaders(),
});

export const GROUPID = "GP01";
// Add a request interceptor
https.interceptors.request.use(
  function (config) {
    console.log(userInforLocal.get()?.accessToken);
    store.dispatch({
      type: BAT_LOAIDNG,
    });

    // Do something before request is sent
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
https.interceptors.response.use(
  function (response) {
    store.dispatch({
      type: TAT_LOADING,
    });

    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
  },
  function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
  }
);
