import { GROUPID, https } from "./url.config";

export const movieService = {
  getBanner: () => {
    return https.get("/api/QuanLyPhim/LayDanhSachBanner");
  },
  getMovies: (params) => {
    let uri = "/api/QuanLyPhim/LayDanhSachPhimPhanTrang";
    return https.get(uri, { params: params });
  },
  getMoviesTheater: () => {
    let uri = "/api/QuanLyRap/LayThongTinLichChieuHeThongRap";
    return https.get(uri);
  },
  getInfoMovieDetail: (maPhim) => {
    let uri = `/api/QuanLyPhim/LayThongTinPhim?MaPhim=${maPhim}`;
    return https.get(uri);
  },
  getInfoDateMovie: (maPhim) => {
    let uri = `/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${maPhim}`;
    return https.get(uri);
  },
  getMovieShowTimeById: (params) => {
    let uri = "/api/QuanLyRap/LayThongTinLichChieuPhim";
    return https.get(uri, { params });
  },
  getInfoBookingMovie: (params) => {
    let uri = "/api/QuanLyDatVe/LayDanhSachPhongVe";
    return https.get(uri, { params });
  },
  postDataBookingChair: (data) => {
    let uri = "/api/QuanLyDatVe/DatVe";
    return https.post(uri, data);
  },
  postConfirmBookingChair: (data) => {
    let uri = "/api/QuanLyDatVe/DatVe";
    return https.post(uri, data);
  },
  getDetailBooking: (maLichChieu) => {
    let uri = `/api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${maLichChieu}`;
    return https.get(uri);
  },
};
