import { movieService } from "../../services/movie.service";
import { SET_CAROUSEL } from "../constants/CaroselConstant";

export const getCarouselAction = () => {
  return async (dispatch) => {
    try {
      const res = await movieService.getBanner();

      dispatch({
        type: SET_CAROUSEL,
        arrImg: res.data.content,
      });
    } catch (err) {
      console.log(err);
    }
  };
};
