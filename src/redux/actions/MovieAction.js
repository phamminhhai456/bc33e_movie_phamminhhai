import { movieService } from "../../services/movie.service";
import {
  SET_DATE_MOVIE,
  SET_DETAIL_MOVIE,
  SET_MOVIE,
  SET_MOVIE_THEATER,
} from "../constants/MovieConstant";

export const getMoviesAction = (params) => {
  return async (dispatch) => {
    try {
      const res = await movieService.getMovies(params);

      dispatch({
        type: SET_MOVIE,
        arrMovies: res.data.content,
      });
    } catch (err) {
      console.log(err);
    }
  };
};

export const getMoviesByTheaterAction = () => {
  return async (dispatch) => {
    try {
      const res = await movieService.getMoviesTheater();
      dispatch({
        type: SET_MOVIE_THEATER,
        arrMoviesTheater: res.data.content,
      });
    } catch (err) {
      console.log(err);
    }
  };
};

export const getDetailMovie = (id) => {
  return async (dispatch) => {
    try {
      const result = await movieService.getInfoMovieDetail(id);
      dispatch({
        type: SET_DETAIL_MOVIE,
        filmDetail: result.data.content,
      });
    } catch (err) {
      console.log(err);
    }
  };
};
export const getDateMovie = (id) => {
  return async (dispatch) => {
    try {
      const result = await movieService.getInfoDateMovie(id);
      dispatch({
        type: SET_DATE_MOVIE,
        filmDate: result.data.content,
      });
    } catch (err) {
      console.log(err);
    }
  };
};
