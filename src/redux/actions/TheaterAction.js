import { theaterService } from "../../services/theater.service";
import { SET_HE_THONG_RAP_CHIEU } from "../constants/TheaterConstant";
export const getDanhSachHeThongRapAction = () => {
  return async (dispatch) => {
    try {
      const res = await theaterService.getTheater();
      console.log("res: ", res);

      if (res.statusCode === 200) {
        dispatch({
          type: SET_HE_THONG_RAP_CHIEU,
          heThongRapChieu: res.data.content,
        });
      }
    } catch (err) {
      console.log(err);
    }
  };
};
