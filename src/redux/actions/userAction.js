import { message } from "antd";
import { userInforLocal } from "../../services/local.service";
import { userServ } from "../../services/user.service";
import { SET_THONG_TIN_NGUOI_DUNG } from "../constants/BookingConstant";
import { SET_LOGIN } from "../constants/userConstant";

export const setLoginActionService = (values, onSuccess) => {
  return (dispatch) => {
    userServ
      .postLogin(values)
      .then((res) => {
        console.log(res);
        userInforLocal.set(res.data.content);
        dispatch({
          type: SET_LOGIN,
          payload: res.data.content,
        });
        onSuccess();
      })
      .catch((err) => {
        message.error(err.response.data.content);
        console.log(err);
      });
  };
};

export const getUserInfor = () => {
  return async (dispatch) => {
    try {
      const result = await userServ.postUserInfo();
      console.log(result);
      if (result.data.statusCode === 200) {
        dispatch({
          type: SET_THONG_TIN_NGUOI_DUNG,
          userInfo: result.data.content,
        });
      }
      console.log(result);
    } catch (err) {
      console.log(err);
    }
  };
};
