import { movieService } from "../../services/movie.service";
import { ThongTinDatVe } from "../../_core/models/ThongTinDatVe";
import {
  DAT_VE_HOAN_TAT,
  SET_CHI_TIET_PHONG_VE,
} from "../constants/BookingConstant";
export const getDetailBookingMovie = (maLichChieu) => {
  return async (dispatch) => {
    try {
      const result = await movieService.getDetailBooking(maLichChieu);
      if (result.status === 200) {
        dispatch({
          type: SET_CHI_TIET_PHONG_VE,
          chiTietPhongVe: result.data.content,
        });
      }
    } catch (err) {
      console.log(err);
      console.log(err.response?.data);
    }
  };
};

export const bookingAction = (thongTinDatVe = new ThongTinDatVe()) => {
  return async (dispatch) => {
    try {
      const result = await movieService.postConfirmBookingChair(thongTinDatVe);
      console.log(result.data);
      await dispatch(getDetailBookingMovie(thongTinDatVe.maLichChieu));
      await dispatch({ type: DAT_VE_HOAN_TAT });
    } catch (err) {
      console.log(err.response?.data);
    }
  };
};
