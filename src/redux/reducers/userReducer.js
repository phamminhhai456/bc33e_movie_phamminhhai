import { userInforLocal } from "../../services/local.service";
import { SET_THONG_TIN_NGUOI_DUNG } from "../constants/BookingConstant";
import { SET_LOGIN } from "../constants/userConstant";

let initialState = {
  userInfor: userInforLocal.get(),
  userInfo: {},
};

export let userReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_LOGIN: {
      state.userInfor = action.payload;
      return { ...state };
    }

    case SET_THONG_TIN_NGUOI_DUNG: {
      state.userInfo = action.userInfo;
      return { ...state };
    }

    default:
      return { ...state };
  }
};
