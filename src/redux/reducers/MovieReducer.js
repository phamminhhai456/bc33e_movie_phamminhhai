import {
  SET_DATE_MOVIE,
  SET_DETAIL_MOVIE,
  SET_MOVIE,
  SET_MOVIE_THEATER,
} from "../constants/MovieConstant";

const stateDefault = {
  arrMovies: [],
  arrMoviesTheater: [],
  filmDetail: {},
  filmDate: {},
};

export const MovieReducer = (state = stateDefault, action) => {
  switch (action.type) {
    case SET_MOVIE: {
      state.arrMovies = action.arrMovies;
      return { ...state };
    }
    case SET_MOVIE_THEATER: {
      state.arrMoviesTheater = action.arrMoviesTheater;
      return { ...state };
    }
    case SET_DETAIL_MOVIE: {
      state.filmDetail = action.filmDetail;
      return { ...state };
    }
    case SET_DATE_MOVIE: {
      state.filmDate = action.filmDate;
      return { ...state };
    }

    default:
      return { ...state };
  }
};
