import { combineReducers } from "redux";
import { userReducer } from "./userReducer";
import { CarouselReducer } from "./CarouselReducer";
import { MovieReducer } from "./MovieReducer";
import { TheaterReducer } from "./TheaterReducer";
import { SpinnerReducer } from "./SpinnerReducer";
import { bookingReducer } from "./bookingReducer";
export const rootReducer = combineReducers({
  userReducer,
  CarouselReducer,
  MovieReducer,
  TheaterReducer,
  SpinnerReducer,
  bookingReducer,
});
