import { BAT_LOAIDNG, TAT_LOADING } from "../constants/SpinnerConstant";
let initialState = {
  isLoading: false,
  count: 0,
};

export const SpinnerReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case BAT_LOAIDNG: {
      state.count++;
      state.isLoading = true;
      return { ...state };
    }
    case TAT_LOADING: {
      state.count--;
      if (state.count === 0) {
        state.isLoading = false;
      }
      return { ...state };
    }
    default:
      return state;
  }
};
