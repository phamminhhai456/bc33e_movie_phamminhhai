import React, { Fragment } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink, useNavigate } from "react-router-dom";
import { SET_LOGIN } from "../../redux/constants/userConstant";
import { userInforLocal } from "../../services/local.service";

export default function UserNav() {
  let userInfor = useSelector((state) => {
    return state.userReducer.userInfor;
  });

  let dispatch = useDispatch();
  let navigate = useNavigate();

  let handleLogout = () => {
    userInforLocal.remove();
    dispatch({
      type: SET_LOGIN,
      payload: null,
    });

    navigate("/");
  };

  let renderContent = () => {
    if (userInfor) {
      return (
        <div className="items-center flex-shrink-0 hidden lg:flex">
          <span className="mr-5 text-blue-500 font-medium text-lg">
            {userInfor.hoTen}
          </span>
          <button
            onClick={handleLogout}
            className="bg-red-500 text-white px-5 py-2 rounded shadow-lg"
          >
            Đăng xuất
          </button>
        </div>
      );
    } else {
      return (
        <div className="items-center flex-shrink-0 hidden lg:flex">
          <NavLink
            to="/login"
            className="self-center px-8 py-3 font-semibold rounded no-underline text-white"
          >
            Đăng Nhập
          </NavLink>
          <NavLink
            to="/register"
            className="self-center px-8 py-3 font-semibold rounded dark:bg-violet-400 dark:text-gray-900 no-underline text-white"
          >
            Đăng Ký
          </NavLink>
        </div>
      );
    }
  };
  return <Fragment>{renderContent()}</Fragment>;
}
