import React from "react";
import { NavLink } from "react-router-dom";
import UserNav from "./UserNav";

export default function Header() {
  return (
    <div className="p-4 bg-coolGray-100 text-coolGray-800 bg-black text-white w-full z-10">
      <div className="container flex justify-between h-16 mx-auto">
        <NavLink
          rel="noopener noreferrer"
          to="/"
          aria-label="Back to homepage"
          className="flex items-center p-2"
        >
          <img
            src="https://cyberlearn.vn/wp-content/uploads/2020/03/cyberlearn-min-new-opt2.png"
            alt="cyberlearn.vn"
          />
        </NavLink>
        <ul className="items-stretch hidden space-x-3 lg:flex">
          <li className="flex">
            <a
              rel="noopener noreferrer"
              href="#lichchieu"
              className="flex items-center font-medium px-4 -mb-1 dark:border-transparent dark:text-violet-400 dark:border-violet-400 text-white no-underline"
              activeClassName="border-b-2 border-white-"
            >
              Lịch Chiếu
            </a>
          </li>
          <li className="flex">
            <a
              rel="noopener noreferrer"
              href="#rapchieu"
              className="flex items-center font-medium px-4 -mb-1 dark:border-transparent text-white no-underline"
              activeClassName="border-b-2 border-white-"
            >
              Cụm Rạp
            </a>
          </li>
          <li className="flex">
            <NavLink
              rel="noopener noreferrer"
              to="/"
              className="flex items-center font-medium px-4 -mb-1 dark:border-transparent text-white no-underline"
              activeClassName="border-b-2 border-white-"
            >
              Tin Tức
            </NavLink>
          </li>
          <li className="flex">
            <a
              rel="noopener noreferrer"
              href="#ungdung"
              className="flex items-center font-medium px-4 -mb-1 dark:border-transparent text-white no-underline"
              activeClassName="border-b-2 border-white-"
            >
              Ứng dụng
            </a>
          </li>
        </ul>
        <UserNav />

        <button className="p-4 lg:hidden">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            className="w-6 h-6 dark:text-gray-100"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M4 6h16M4 12h16M4 18h16"
            />
          </svg>
        </button>
      </div>
    </div>
  );
}
