import React from "react";
import { useSelector } from "react-redux";
import { PacmanLoader } from "react-spinners";

export default function Spinner() {
  let { isLoading } = useSelector((state) => {
    return state.SpinnerReducer;
  });
  return isLoading ? (
    <div
      style={{ height: "100vh" }}
      className="fixed w-screen  h-screen flex justify-center items-center bg-black top-0 left-0 z-50"
    >
      <PacmanLoader color="#7E22CD" margin={3} size={45} />
    </div>
  ) : (
    <></>
  );
}
