import { Button, Rate, Tabs } from "antd";
import TabPane from "antd/lib/tabs/TabPane";
import moment from "moment";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import { getDateMovie, getDetailMovie } from "../../redux/actions/MovieAction";
import "../../assets/styles/circle.scss";
import styled from "./Detail.module.css";
import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";
import Swal from "sweetalert2";

export default function DetailPage() {
  const filmDetail = useSelector((state) => state.MovieReducer.filmDetail);

  const filmDate = useSelector((state) => state.MovieReducer.filmDate);

  let userInfo = useSelector((state) => state.userReducer.userInfor);
  let dispatch = useDispatch();
  let { id } = useParams();
  let navigate = useNavigate();
  useEffect(() => {
    dispatch(getDetailMovie(id));
    dispatch(getDateMovie(id));
  }, []);

  return (
    <div>
      <Header />
      <div
        style={{
          backgroundImage: `url(${filmDetail.hinhAnh})`,
          backgroundSize: "100%",
          backgroundPositionY: "center",
          minHeight: "100vh",
        }}
      >
        <div
          className={styled.box1}
          style={{ paddingTop: 150, minHeight: "100vh", color: "#fff" }}
        >
          <div className="grid grid-cols-12">
            <div className="col-span-7 col-start-3">
              <div className="grid grid-cols-4 gap-5">
                <img
                  className="col-span-1"
                  src={filmDetail.hinhAnh}
                  style={{ width: 250, height: 300 }}
                  alt="123"
                />
                <div
                  className="col-span-2"
                  col-start-3
                  style={{ marginTop: "25%" }}
                >
                  <p className="text-lg text-black">
                    Ngày chiếu:{" "}
                    {moment(filmDetail.ngayKhoiChieu).format("DD.MM.YYYY")}
                  </p>
                  <p className="text-4xl leading-1 text-black">
                    {filmDetail.tenPhim}
                  </p>
                </div>
              </div>
            </div>

            <div className="col-span-3">
              <h1
                style={{
                  marginLeft: "16%",
                  color: "yellow",
                  fontWeight: "bold",
                  fontSize: 15,
                }}
              >
                Đánh giá
              </h1>
              <h1
                style={{ marginLeft: "5%" }}
                className="text-green-400 text-2xl"
              >
                <Rate
                  allowHalf
                  value={filmDetail.danhGia / 2}
                  style={{ color: "#78ed78", fontSize: 30 }}
                />
              </h1>
              <div className={`c100 p${filmDetail.danhGia * 10} big`}>
                <span className="text-white">{filmDetail.danhGia * 10}%</span>
                <div className="slice">
                  <div className="bar"></div>
                  <div className="fill"></div>
                </div>
              </div>
              <br />
            </div>
          </div>
          <div className="mt-10 w-1/2 container bg-white px-5 py-5 mx-auto">
            <Tabs defaultActiveKey="1" centered>
              <TabPane tab="Lịch chiếu" key="1" style={{ minHeight: 300 }}>
                <div>
                  <Tabs tabPosition={"left"}>
                    {filmDate.heThongRapChieu?.map((htr, index) => {
                      return (
                        <TabPane
                          tab={
                            <div className="flex flex-row items-center justify-center">
                              <img
                                src={htr.logo}
                                className="rounded-full w-full"
                                style={{ width: 50 }}
                                alt="..."
                              />
                              <div className="text-center ml-2">
                                {htr.tenHeThongRap}
                              </div>
                            </div>
                          }
                          key={index}
                        >
                          {htr.cumRapChieu?.map((cumRap, index) => {
                            return (
                              <div className="mt-5" key={index}>
                                <div className="flex flex-row">
                                  <img
                                    style={{ width: 60, height: 60 }}
                                    src={cumRap.hinhAnh}
                                    alt="..."
                                  />
                                  <div className="ml-2">
                                    <p
                                      style={{
                                        fontSize: 20,
                                        fontWeight: "bold",
                                        lineHeight: 1,
                                      }}
                                    >
                                      {cumRap.tenCumRap}
                                    </p>
                                    <p
                                      className="text-gray-400"
                                      style={{ marginTop: 0 }}
                                    >
                                      {cumRap.diaChi}
                                    </p>
                                  </div>
                                </div>
                                <div className="thong-tin-lich-chieu grid grid-cols-4">
                                  {cumRap.lichChieuPhim
                                    ?.slice(0, 10)
                                    .map((lichChieu, index) => {
                                      return (
                                        <Button
                                          onClick={() => {
                                            if (userInfo) {
                                              window.location.href = `/checkout/${lichChieu.maLichChieu}`;
                                            } else {
                                              navigate("/login");
                                              Swal.fire({
                                                icon: "error",
                                                title:
                                                  "Vui lòng đăng nhập để được mua vé",
                                                showConfirmButton: false,
                                                timer: 2000,
                                              });
                                            }
                                          }}
                                          key={index}
                                          className="col-span-1 text-green-800 font-bold mt-2 mr-2"
                                        >
                                          {moment(
                                            lichChieu.ngayChieuGioChieu
                                          ).format("hh:mm A")}
                                        </Button>
                                      );
                                    })}
                                </div>
                              </div>
                            );
                          })}
                        </TabPane>
                      );
                    })}
                  </Tabs>
                </div>
              </TabPane>
              <TabPane tab="Thông tin" key="2" style={{ minHeight: 300 }}>
                <div className="grid grid-cols-6 gap-4">
                  <div className="col-start-1 col-end-2">
                    <p>Ngày công chiếu</p>
                    <p className="mt-3">Tên Phim: </p>
                  </div>
                  <div className="col-start-2 col-end-4">
                    <p>
                      {moment(filmDetail.ngayKhoiChieu).format("DD.MM.YYYY")}
                    </p>
                    <p className="mt-9">{filmDetail.tenPhim} </p>
                  </div>
                  <div className="col-start-4 col-end-9">
                    <p>Nội dung</p>
                    <p className="mt-4">
                      {filmDetail.moTa?.length > 1000
                        ? `${filmDetail.moTa.slice(0, 1000)}...`
                        : filmDetail.moTa}{" "}
                    </p>
                  </div>
                </div>
              </TabPane>
              <TabPane tab="Đánh giá" key="3" style={{ minHeight: 300 }}>
                Đánh giá
              </TabPane>
            </Tabs>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}
