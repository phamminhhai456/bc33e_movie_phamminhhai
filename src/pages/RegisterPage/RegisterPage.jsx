import { message } from "antd";
import React from "react";
import { useState } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import { backGroundLogin } from "../../assets/img/linkbg";
import { userServ } from "../../services/user.service";

export default function RegisterPage() {
  let navigate = useNavigate();
  let [valueInput, setValueInput] = useState({
    taiKhoan: "",
    matKhau: "",
    email: "",
    soDienThoai: "",
    hoTen: "",
  });
  let [errValueInput, setErrValueInput] = useState({
    taiKhoan: "",
    matKhau: "",
    email: "",
    soDienThoai: "",
    hoTen: "",
  });
  let handleChangeValueInput = (e) => {
    let { value, name } = e.target;

    let checkRegex = (regex, message) => {
      if (regex.test(value)) {
        setErrValueInput({ ...errValueInput, [name]: "" });
      } else {
        setErrValueInput({ ...errValueInput, [name]: message });
      }
    };

    if (value.trim() === "") {
      setErrValueInput({
        ...errValueInput,
        [name]: ` * Vui lòng điền thông tin`,
      });
    } else {
      setErrValueInput({
        ...errValueInput,
        [name]: ``,
      });
    }
    if (value.length >= 1) {
      switch (name) {
        case "taiKhoan":
          {
            checkRegex(
              /^[a-zA-Z0-9_]*$/,
              "Không được chứa khoảng trắng và ký tự đặc biệt"
            );
          }
          break;
        case "matKhau":
          {
            checkRegex(
              /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/,
              "Tối thiểu tám ký tự, ít nhất một chữ cái và một số, không chứa kí tự đặc biệt"
            );
          }
          break;
        case "email":
          {
            checkRegex(
              /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
              "Email chưa đúng định dạng"
            );
          }
          break;
        case "soDienThoai":
          {
            checkRegex(
              /(84|0[3|5|7|8|9])+([0-9]{8})+$\b/,
              "Vui lòng nhập đúng số điện thoại"
            );
          }
          break;
        case "hoTen": {
          checkRegex(
            /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/,
            "Họ tên phải là chữ"
          );
        }
      }
    }
    setValueInput({ ...valueInput, [name]: value });
  };

  let handleSubmit = (e) => {
    e.preventDefault();
    let isValid = true;
    for (let key in valueInput) {
      if (valueInput[key] === "") {
        isValid = false;
      }
    }
    for (let key in errValueInput) {
      if (errValueInput[key] !== "") {
        isValid = false;
      }
    }
    if (isValid) {
      let handleRegister = async (user) => {
        try {
          let res = await userServ.postRegister(user);
          console.log(res);
          message.success("Đăng kí thành công");
          navigate("/login");
        } catch (err) {
          if (err.response.data.content === "Email đã tồn tại!") {
            setErrValueInput({ ...errValueInput, email: "Email đã tồn tại!" });
          } else {
            setErrValueInput({
              ...errValueInput,
              taiKhoan: "Tài khoản đã tồn tại!",
            });
          }
          message.error("Đăng Kí thất bại");
        }
      };
      handleRegister(valueInput);
    } else {
      message.error("đăng kí thất bại");
    }
  };
  return (
    <div
      style={{
        backgroundImage: `url(${backGroundLogin})`,
        backgroundSize: "150%",
        backgroundPosition: "center",
      }}
      className="relative w-screen h-screen"
    >
      <div
        onClick={() => {
          setTimeout(() => {
            navigate("/");
          }, 500);
        }}
        className="absolute  top-5 left-5  text-5xl   text-purple-700 font-extrabold cursor-pointer opacity-70 z-50"
      >
        <img
          src="https://cyberlearn.vn/wp-content/uploads/2020/03/cyberlearn-min-new-opt2.png"
          alt="cyberlearn.vn"
        />
      </div>
      <div className="bg-grey-lighter min-h-screen flex flex-col">
        <div className="container max-w-sm mx-auto flex-1 flex flex-col items-center justify-center px-2">
          <div className="bg-white px-6 py-10 rounded shadow-md text-black w-full">
            <h1 className="mb-8 text-3xl text-center ">Đăng Ký</h1>
            <input
              value={valueInput.taiKhoan}
              onChange={(e) => {
                handleChangeValueInput(e);
              }}
              type="text"
              className="block border border-grey-light w-full p-3 rounded "
              name="taiKhoan"
              placeholder="Tài Khoản"
            />
            <span className="text-red-500 text-xs mb-4">
              {errValueInput.taiKhoan}
            </span>
            <input
              value={valueInput.matKhau}
              onChange={(e) => {
                handleChangeValueInput(e);
              }}
              type="password"
              className="block border border-grey-light w-full p-3 rounded my-2"
              name="matKhau"
              placeholder="Mật Khẩu"
            />
            <span className="text-red-500 text-xs">
              {errValueInput.matKhau}
            </span>
            <input
              value={valueInput.email}
              onChange={(e) => {
                handleChangeValueInput(e);
              }}
              type="text"
              className="block border border-grey-light w-full p-3 rounded my-2"
              name="email"
              placeholder="Email"
            />
            <span className="text-red-500 text-xs">{errValueInput.email}</span>
            <input
              value={valueInput.soDienThoai}
              onChange={(e) => {
                handleChangeValueInput(e);
              }}
              type="text"
              className="block border border-grey-light w-full p-3 rounded my-2"
              name="soDienThoai"
              placeholder="Số Điện Thoại"
            />
            <span className="text-red-500 text-xs">
              {errValueInput.soDienThoai}
            </span>
            <input
              value={valueInput.hoTen}
              onChange={(e) => {
                handleChangeValueInput(e);
              }}
              type="text"
              className="block border border-grey-light w-full p-3 rounded my-2"
              name="hoTen"
              placeholder="Họ Tên"
            />
            <span className="text-red-500 text-xs">{errValueInput.hoTen}</span>

            <button
              type="submit"
              onClick={(e) => {
                handleSubmit(e);
              }}
              className="w-full text-center py-3 rounded bg-green-400 text-white hover:bg-green-700 focus:outline-none my-2"
            >
              Tạo Tài Khoản
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
