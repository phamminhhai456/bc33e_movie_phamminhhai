import { Button, Form, Input, message } from "antd";
import Lottie from "lottie-react";
import React from "react";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { NavLink, useNavigate } from "react-router-dom";
import Bg_Animate from "../../assets/bg_login.json";
import { setLoginActionService } from "../../redux/actions/userAction";
import { SET_LOGIN } from "../../redux/constants/userConstant";
import { userInforLocal } from "../../services/local.service";
import { userServ } from "../../services/user.service";
export default function LoginPage() {
  let dispatch = useDispatch();

  let navigate = useNavigate();

  useEffect(() => {
    let userInfor = userInforLocal.get();
    if (userInfor) {
      navigate("/");
    }
  }, []);

  const onFinish = (values) => {
    let handleSuccess = () => {
      message.success("Đăng nhập thành công");
      setTimeout(() => {
        navigate("/");
      }, 500);
    };
    dispatch(setLoginActionService(values, handleSuccess));
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div
      style={{ height: "100vh" }}
      className="h-screen w-screen bg-black flex justify-center items-center px-20"
    >
      <div className="container mx-auto p-5 bg-white rounded flex">
        <div
          onClick={() => {
            setTimeout(() => {
              navigate("/");
            }, 500);
          }}
          className="absolute top-5 left-5  text-5xl text-purple-700 font-extra bold cursor-pointer opacity-70 z-50"
        >
          <img
            src="https://cyberlearn.vn/wp-content/uploads/2020/03/cyberlearn-min-new-opt2.png"
            alt="cyberlearn.vn"
          />
        </div>
        <div className="w-1/2 transform scale-50">
          {/* animate */}
          <Lottie animationData={Bg_Animate} />
        </div>
        <div className="w-1/2 flex items-center justify-center">
          <Form
            className="w-full"
            layout="vertical"
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label="Tài khoản"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập thông tin",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Mật khẩu"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập thông tin",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item>
              <p>
                Bạn chưa có tài khoản ?{" "}
                <NavLink to="/register" className="text-blue-500">
                  Đăng Ký Ngay
                </NavLink>
              </p>
            </Form.Item>

            <Form.Item
              wrapperCol={{
                offset: 0,
                span: 24,
              }}
            >
              <Button
                className="w-full bg-red-500 text-white border-none"
                htmlType="submit"
              >
                Đăng Nhập
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
}
