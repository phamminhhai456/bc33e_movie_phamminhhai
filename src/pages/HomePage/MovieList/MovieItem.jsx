import React from "react";
import "./ItemMovie.sass";
import { useNavigate } from "react-router-dom";
import MovieItemVideo from "./MovieItemVideo";
import { useMediaQuery } from "react-responsive";

export default function MovieItem(props) {
  let navigate = useNavigate();
  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 1224px)" });
  const { hinhAnh, tenPhim, maPhim, trailer } = props.item;
  return (
    <div
      className="movie mx-auto"
      style={{ backgroundImage: `url(${hinhAnh})` }}
    >
      <h2 className="movie__title ">{tenPhim}</h2>

      <div className="movie__play">
        <MovieItemVideo trailer={trailer} />
      </div>

      <div className="movie__button__datve">
        <button
          onClick={() => {
            navigate(`/detail/${maPhim}`);
          }}
        >
          Chi Tiết Phim
        </button>
      </div>
    </div>
  );
}
