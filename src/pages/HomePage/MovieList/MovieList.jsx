import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getMoviesAction } from "../../../redux/actions/MovieAction";
import styleSlick from "./MovieSlick.module.css";

import MovieItem from "./MovieItem";
import Slider from "react-slick";
import {
  SET_FILM_DANG_CHIEU,
  SET_FILM_SAP_CHIEU,
} from "../../../redux/constants/MovieConstant";

function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={`${className} ${styleSlick["slick-prev"]}`}
      style={{ ...style, display: "block" }}
      onClick={onClick}
    ></div>
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={`${className} ${styleSlick["slick-prev"]}`}
      style={{ ...style, display: "block", left: "-50px" }}
      onClick={onClick}
    ></div>
  );
}
const settings = {
  className: "center variable-width",
  centerMode: true,
  infinite: true,
  centerPadding: "60px",
  slidesToShow: 2,
  speed: 500,
  rows: 2,
  slidesPerRow: 2,
  variableWidth: true,
  nextArrow: <SampleNextArrow />,
  prevArrow: <SamplePrevArrow />,
};

export default function MovieList() {
  const { arrMovies } = useSelector((state) => state.MovieReducer);

  let dispatch = useDispatch();

  useEffect(() => {
    let params = {
      maNhom: "GP09",
      soTrang: "1",
      soPhanTuTrenTrang: 12,
    };
    dispatch(getMoviesAction(params));
  }, []);

  const renderFilms = () => {
    return arrMovies.items?.map((item) => {
      return (
        <div key={item.maPhim}>
          <MovieItem item={item} />;
        </div>
      );
    });
  };

  return (
    <div className="container mx-auto mt-4 " id="lichchieu">
      <div className="grid grid-cols-1 gap-6">
        <Slider {...settings}>{renderFilms()}</Slider>
      </div>
    </div>
  );
}
