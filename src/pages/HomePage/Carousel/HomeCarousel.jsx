import { Carousel } from "antd";
import React, { useRef } from "react";
import { RightOutlined, LeftOutlined } from "@ant-design/icons";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getCarouselAction } from "../../../redux/actions/CarouselAction";
import "../Carousel/HomeCarousel.css";
import "react-responsive-carousel/lib/styles/carousel.min.css";

const contentStyle = {
  height: "600px",
  color: "#fff",
  lineHeight: "160px",
  textAlign: "center",
  backgroundPosition: "center",
  backgroundSize: "100%",
  backgroundRepeat: "no-repeat",
};
export default function HomeCarousel(props) {
  const { arrImg } = useSelector((state) => state.CarouselReducer);
  let ref = useRef();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getCarouselAction());
  }, []);
  const onChange = (currentSlide) => {
    console.log(currentSlide);
  };
  return (
    <div className="w-full relative">
      <Carousel
        ref={ref}
        dots={false}
        className="a"
        afterChange={onChange}
        style={{ width: "100%", padding: 0, margin: 0 }}
      >
        {arrImg.map((item) => {
          return (
            <div className="w-full" key={item.maBanner}>
              <div
                style={{
                  ...contentStyle,
                  backgroundImage: `url(${item.hinhAnh})`,
                }}
              >
                <img
                  src={item.hinhAnh}
                  alt={item.hinhAnh}
                  className="opacity-0"
                />
              </div>
            </div>
          );
        })}
      </Carousel>
      <button
        className="text-white absolute left-0 top-1/2"
        onClick={() => {
          ref.current.prev();
        }}
      >
        <LeftOutlined
          className="transition ease-in-out text-stone-300 hover:text-blue-500 hover:scale-150 duration-300"
          style={{ fontSize: "3rem" }}
        />
      </button>
      <button
        className="text-white absolute right-0 top-1/2"
        onClick={() => {
          ref.current.next();
        }}
      >
        <RightOutlined
          className="transition ease-in-out text-stone-300 hover:text-blue-500 hover:scale-150 duration-300"
          style={{ fontSize: "3rem" }}
        />
      </button>
    </div>
  );
}
