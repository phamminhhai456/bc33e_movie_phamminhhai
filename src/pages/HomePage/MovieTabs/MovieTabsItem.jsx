import moment from "moment";
import React from "react";
import { NavLink } from "react-router-dom";

export default function MovieTabsItem({ movie }) {
  return (
    <div className="flex p-3 items-center">
      <img className="h-32 w-24 object-cover" src={movie.hinhAnh} alt="" />
      <div className="ml-5 ">
        <p className="text-left text-xl font-medium">{movie.tenPhim}</p>
        <div className="grid grid-cols-3 gap-5">
          {movie.lstLichChieuTheoPhim.slice(0, 6).map((item) => {
            return (
              <NavLink
                key={item.maLichChieu}
                to="/"
                className="p-3 bg-red-500 rounded text-white"
              >
                {moment(item.ngayChieuGioChieu).format("DD/YY - HH/MM")}
              </NavLink>
            );
          })}
        </div>
      </div>
    </div>
  );
}
