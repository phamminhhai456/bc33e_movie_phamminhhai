import { Tabs } from "antd";
import React, { useState } from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useMediaQuery } from "react-responsive";
import { getMoviesByTheaterAction } from "../../../redux/actions/MovieAction";
import { movieService } from "../../../services/movie.service";
import MovieTabsItem from "./MovieTabsItem";

export default function MovieTabs() {
  let { arrMoviesTheater } = useSelector((state) => state.MovieReducer);
  let dispatch = useDispatch();
  const isDesktopOrLaptop = useMediaQuery({ query: "(min-width: 1224px)" });
  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 1224px)" });
  const handleTabResponesive = () => {
    if (isDesktopOrLaptop) {
      return "left";
    }
    if (isTabletOrMobile) {
      return "top";
    }
  };
  useEffect(() => {
    dispatch(getMoviesByTheaterAction());
  }, []);

  let renderHeThongRap = () => {
    return arrMoviesTheater.map((heThongRap, index) => {
      return (
        <Tabs.TabPane
          tab={
            <img
              className="lg:w-20 lg:h-20 md:w-16 md:h-16 w-10 h-10"
              src={heThongRap.logo}
              alt=""
            />
          }
          key={index}
        >
          <div className="h-100">
            <Tabs tabPosition={handleTabResponesive()} defaultActiveKey="1">
              {heThongRap.lstCumRap.map((cumRap) => {
                return (
                  <Tabs.TabPane
                    key={cumRap.maCumRap}
                    tab={
                      <div className="text-lg text-black hover:text-blue-500 ">
                        {cumRap.tenCumRap}
                      </div>
                    }
                  >
                    <div className="movie-tabs__contents h-100 overflow-y-scroll p-3">
                      <div className="h-full overflow-auto">
                        {cumRap.danhSachPhim.slice(0, 12).map((phim) => {
                          return (
                            <MovieTabsItem key={phim.maPhim} movie={phim} />
                          );
                        })}
                      </div>
                    </div>
                  </Tabs.TabPane>
                );
              })}
            </Tabs>
          </div>
        </Tabs.TabPane>
      );
    });
  };
  return (
    <div className="py-10" id="rapchieu">
      <div className="movie-tabs container mx-auto my-10 h-100 overflow-y-scroll">
        <div
          style={{
            border: "1px solid white",
          }}
          className="w-full h-full"
        >
          <Tabs tabPosition={handleTabResponesive()} defaultActiveKey="1">
            {renderHeThongRap()}
          </Tabs>
        </div>
      </div>
    </div>
  );
}
