import React from "react";
import Footer from "../../components/Footer/Footer";
import Header from "../../components/Header/Header";
import HomeCarousel from "./Carousel/HomeCarousel";
import MovieList from "./MovieList/MovieList";
import MovieTabs from "./MovieTabs/MovieTabs";
import ButtonToTop from "../../components/ButtonToTop/ButtonToTop";
export default function HomePage() {
  return (
    <div className="scroll-smooth">
      <Header />
      <HomeCarousel />
      <MovieList />
      <MovieTabs />
      <Footer />
      <ButtonToTop />
    </div>
  );
}
