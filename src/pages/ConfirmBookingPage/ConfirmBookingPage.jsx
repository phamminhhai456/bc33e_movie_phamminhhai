import React from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { getUserInfor } from "../../redux/actions/userAction";

export default function ConfirmBookingPage() {
  const dispatch = useDispatch();
  const { userInfo, userInfor } = useSelector((state) => state.userReducer);
  let navigate = useNavigate();

  useEffect(() => {
    if (!userInfor) {
      navigate("/");
    }
  }, [userInfor]);

  //   useEffect(() => {
  //     const action = getUserInfor();
  //     dispatch(action);
  //   }, []);
  console.log("userInfo: ", userInfo);
  return (
    <div className="py-5">
      {" "}
      <section className="text-gray-600 body-font">
        <div className="container px-5 py-24 mx-auto">
          <div className="flex flex-col text-center w-full mb-20">
            <h1 className="sm:text-3xl text-2xl font-medium title-font mb-4  text-purple-600 ">
              Lịch sử đặt vé khách hàng
            </h1>
            <p className="lg:w-2/3 mx-auto leading-relaxed text-base">
              Hãy xem thông tin địa và thời gian để xem phim vui vẻ bạn nhé !
            </p>
          </div>
          <div className="flex flex-wrap -m-2">
            <div className="p-2 lg:w-1/3 md:w-1/2 w-full">
              <div className="h-full flex items-center border-gray-200 border p-4 rounded-lg">
                <img
                  alt="team"
                  className="w-16 h-16 bg-gray-100 object-cover object-center flex-shrink-0 rounded-full mr-4"
                  src="https://picsum.photos/200/200"
                />
                <div className="flex-grow">
                  <h2 className="text-gray-900 title-font font-medium">
                    Lật mặt 48h
                  </h2>
                  <p className="text-gray-500">
                    10:20 Rạp 5, Hệ thống rạp cinestar bhd{" "}
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
