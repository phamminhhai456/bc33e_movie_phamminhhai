import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import Header from "../../components/Header/Header";
import { getDetailBookingMovie } from "../../redux/actions/BookingAction";
import { movieService } from "../../services/movie.service";
import BookingChair from "./BookingChair";
import BookingInfo from "./BookingInfo";

export default function Checkout() {
  let { id } = useParams();
  let navigate = useNavigate();
  let dispatch = useDispatch();
  let userInfor = useSelector((state) => state.userReducer.userInfor);
  const { chiTietPhongVe } = useSelector((state) => state.bookingReducer);
  useEffect(() => {
    if (!userInfor) {
      navigate("/");
      setTimeout(() => {
        Swal.fire({
          icon: "error",
          title: "Vui lòng đăng nhập để được mua vé",
          showConfirmButton: false,
          timer: 2000,
        });
      }, 500);
    }
  }, [userInfor]);

  useEffect(() => {
    const action = getDetailBookingMovie(id);
    dispatch(action);
  }, []);

  const { thongTinPhim, danhSachGhe } = chiTietPhongVe;

  return (
    <div>
      <Header />
      <div className="min-h-screen mt-5">
        <div className="grid grid-cols-12">
          <BookingChair danhSachGhe={danhSachGhe} />
          <BookingInfo thongTinPhim={thongTinPhim} maLichChieu={id} />
        </div>
      </div>
    </div>
  );
}
