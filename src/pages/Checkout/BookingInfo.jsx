import _ from "lodash";
import React from "react";
import { ThongTinDatVe } from "../../_core/models/ThongTinDatVe";
import { useDispatch, useSelector } from "react-redux";
import { bookingAction } from "../../redux/actions/BookingAction";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

export default function BookingInfo({ thongTinPhim }) {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  const { userInfor } = useSelector((state) => state.userReducer);
  const { danhSachGheDangDat } = useSelector((state) => state.bookingReducer);
  return (
    <div className="col-span-3">
      <h3 className="text-green-400 text-center text-4xl">
        {danhSachGheDangDat
          .reduce((tongTien, ghe, index) => {
            return (tongTien += ghe.giaVe);
          }, 0)
          .toLocaleString()}{" "}
        đ
      </h3>
      <hr />
      <h3 className="text-xl mt-2">{thongTinPhim.tenPhim}</h3>
      <p>
        Địa điểm: {thongTinPhim.tenCumRap} - {thongTinPhim.tenRap}
      </p>
      <p>
        Ngày chiếu: {thongTinPhim.ngayChieu} - {thongTinPhim.gioChieu}
      </p>
      <hr />
      <div className="my-5">
        <div className="w-4/5">
          <span className="text-red-400 text-lg">Ghế</span>
          {_.sortBy(danhSachGheDangDat, ["stt"]).map((gheDD, index) => {
            return (
              <span key={index} className="text-green-500 text-xl">
                {" "}
                {gheDD.stt}
              </span>
            );
          })}
        </div>
        <div className="flex flex-row">
          <p className="text-lg"> Tổng tiền :</p>
          <span className="text-green-800 text-lg ml-3">
            {danhSachGheDangDat
              .reduce((tongTien, ghe, index) => {
                return (tongTien += ghe.giaVe);
              }, 0)
              .toLocaleString()}{" "}
            đ
          </span>
        </div>
      </div>
      <hr />
      <div className="my-5">
        <i>
          E-mail <br />
        </i>
        {userInfor.email}
      </div>
      <div className="my-5">
        <i>
          Phone <br />
        </i>
        {userInfor.soDT}
      </div>
      <div
        className="mb-0 h-full flex flex-col items-center"
        style={{ marginBottom: 0 }}
      >
        <div
          onClick={() => {
            const thongTinDatVe = new ThongTinDatVe();
            thongTinDatVe.maLichChieu = thongTinPhim.maLichChieu;
            thongTinDatVe.danhSachVe = danhSachGheDangDat;
            setTimeout(() => {
              Swal.fire({
                icon: "success",
                title: "Đặt vé thành công",
                showConfirmButton: false,
                timer: 2000,
              });
            }, 700);
            dispatch(bookingAction(thongTinDatVe));
            navigate("/confirm");
          }}
          className="bg-green-500 text-white w-full text-center py-3 font-bold text-2xl cursor-pointer"
        >
          ĐẶT VÉ
        </div>
      </div>
    </div>
  );
}
