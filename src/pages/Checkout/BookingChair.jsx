import React, { Fragment } from "react";
import { DAT_VE } from "../../redux/constants/BookingConstant";
import {
  CheckOutlined,
  CloseOutlined,
  UserOutlined,
  SmileOutlined,
} from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import "./Checkout.css";
import style from "./Checkout.module.css";
export default function BookingChair({ danhSachGhe }) {
  let dispatch = useDispatch();
  const { danhSachGheDangDat, danhSachGheKhachDat } = useSelector(
    (state) => state.bookingReducer
  );
  const { userInfor } = useSelector((state) => state.userReducer);

  const renderSeat = () => {
    return danhSachGhe.map((ghe, index) => {
      let classGheVip = ghe.loaiGhe === "Vip" ? "gheVip" : "";
      let classGheDaDat = ghe.daDat === true ? "gheDaDat" : "";
      let classGheDangDat = "";

      let indexGheDD = danhSachGheDangDat.findIndex(
        (gheDD) => gheDD.maGhe === ghe.maGhe
      );

      let classGheKhackDat = "";

      let indexGheKD = danhSachGheKhachDat.findIndex(
        (gheKD) => gheKD.maGhe === ghe.maGhe
      );

      if (indexGheKD !== -1) {
        classGheKhackDat = "gheKhachDat";
      }

      let classGheDaDuocDat = "";
      if (userInfor.taiKhoan === ghe.taiKhoanNguoiDat) {
        classGheDaDuocDat = "gheDaDuocDat";
      }

      if (indexGheDD !== -1) {
        classGheDaDat = "gheDangDat";
      }
      return (
        <Fragment key={index}>
          <button
            onClick={() => {
              dispatch({
                type: DAT_VE,
                gheDuocChon: ghe,
              });
            }}
            disabled={ghe.daDat}
            className={`ghe ${classGheVip} ${classGheDaDat} ${classGheDangDat}  ${classGheDaDuocDat} ${classGheKhackDat} text-center`}
            key={index}
          >
            {ghe.daDat ? (
              classGheDaDuocDat !== "" ? (
                <UserOutlined
                  style={{ marginBottom: 7.5, fontWeight: "bold" }}
                />
              ) : (
                <CloseOutlined
                  style={{ marginBottom: 7.5, fontWeight: "bold" }}
                />
              )
            ) : classGheKhackDat !== "" ? (
              <SmileOutlined
                style={{ marginBottom: 7.5, fontWeight: "bold" }}
              />
            ) : (
              ghe.stt
            )}
          </button>
          {(index + 1) % 16 === 0 ? <br /> : ""}
        </Fragment>
      );
    });
  };
  return (
    <div className="col-span-9">
      <div className="flex flex-col items-center mt-5">
        <div className="bg-black " style={{ width: "80%", height: 15 }}></div>
        <div className={`${style["trapezoid"]} text-center`}>
          <h3 className="mt-3 text-black">Màn hình</h3>
        </div>
        <div>{renderSeat()}</div>
      </div>
      <div className="mt-5 flex justify-center">
        <table className=" divide-y divide-gray-200 w-2/3">
          <thead className="bg-gray-50 p-5">
            <tr>
              <th>Ghế chưa đặt</th>
              <th>Ghế đang đặt</th>
              <th>Ghế vip</th>
              <th>Ghế đã đặt</th>
              <th>Ghế mình đặt</th>
              <th>Ghế khách đang đặt</th>
            </tr>
          </thead>
          <tbody className="bg-white divide-y divide-gray-200">
            <tr>
              <td>
                <button className="ghe text-center">
                  {" "}
                  <CheckOutlined
                    style={{ marginBottom: 7.5, fontWeight: "bold" }}
                  />{" "}
                </button>{" "}
              </td>
              <td>
                <button className="ghe gheDangDat text-center">
                  {" "}
                  <CheckOutlined
                    style={{ marginBottom: 7.5, fontWeight: "bold" }}
                  />
                </button>{" "}
              </td>
              <td>
                <button className="ghe gheVip text-center">
                  <CheckOutlined
                    style={{ marginBottom: 7.5, fontWeight: "bold" }}
                  />
                </button>{" "}
              </td>
              <td>
                <button className="ghe gheDaDat text-center">
                  {" "}
                  <CheckOutlined
                    style={{ marginBottom: 7.5, fontWeight: "bold" }}
                  />{" "}
                </button>{" "}
              </td>
              <td>
                <button className="ghe gheDaDuocDat text-center">
                  {" "}
                  <CheckOutlined
                    style={{ marginBottom: 7.5, fontWeight: "bold" }}
                  />{" "}
                </button>{" "}
              </td>
              <td>
                <button className="ghe gheKhachDat text-center">
                  {" "}
                  <CheckOutlined
                    style={{ marginBottom: 7.5, fontWeight: "bold" }}
                  />{" "}
                </button>{" "}
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
}
