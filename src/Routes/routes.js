import Checkout from "../pages/Checkout/Checkout";
import ConfirmBookingPage from "../pages/ConfirmBookingPage/ConfirmBookingPage";
import DetailPage from "../pages/DetailPage/DetailPage";
import HomePage from "../pages/HomePage/HomePage";
import LoginPage from "../pages/LoginPage/LoginPage";
import RegisterPage from "../pages/RegisterPage/RegisterPage";

export const routes = [
  {
    path: "/",
    component: <HomePage />,
  },
  {
    path: "/login",
    component: <LoginPage />,
  },
  {
    path: "/detail/:id",
    component: <DetailPage />,
  },
  {
    path: "/register",
    component: <RegisterPage />,
  },
  {
    path: "/checkout/:id",
    component: <Checkout />,
  },
  {
    path: "/confirm",
    component: <ConfirmBookingPage />,
  },
];
